
<div class="row">
	<div class='col-6'><h3>Mis tareas</h3></div>
	<div class="col-6">
		<button type="button" class="btn btn-success float-right" data-toggle="modal" data-target="#addTask" ng-click='newTask()'>
		 <i class="fas fa-plus-circle"></i> Nueva tarea
		</button>
	</div>
</div>

<ul class="list-group" >
<li ng-repeat="tarea in tasks"  class="list-group-item clearfix task"  >
	<strong>Tarea: </strong>{{tarea.name}} <br/>
	<strong>Vencimiento: </strong> <span class="badge badge-primary badge-pill">{{tarea.expiration_date}}</span><br/>
	<strong>Prioridad:</strong>

	<span class="badge badge-success badge-pill" ng-show="tarea.priority=='Baja'">{{tarea.priority}}</span>
	<span class="badge badge-warning badge-pill" ng-show="tarea.priority=='Media'">{{tarea.priority}}</span>
	<span class="badge badge-danger badge-pill" ng-show="tarea.priority=='Alta'">{{tarea.priority}}</span>

	<span class="badge badge-danger float-right" ng-show="convertirFecha( tarea.expiration_date ) < now" >Vencida ({{daydiff(convertirFecha( tarea.expiration_date ) , now)}} días) </span>
	<span class="badge badge-warning float-right" ng-show="daydiff( convertirFecha( tarea.expiration_date ), now) < 10 && daydiff( convertirFecha( tarea.expiration_date ), now) >0" >Próximas a vencer ({{daydiff(convertirFecha( tarea.expiration_date ) , now)}} días) </span>
	<span class="badge badge-success float-right" ng-show="daydiff( convertirFecha( tarea.expiration_date ), now) >15" >Programada ({{daydiff(convertirFecha( tarea.expiration_date ) , now)}} días) </span>
	<br/>
	{{tarea.description}}

	<div>
				<span class="float-right">
					<button class="btn btn-default btn-xs" ng-click="editTask( tarea.id )"  data-toggle="modal" data-target="#addTask">
						<span class="fas fa-pencil-alt"
						>
						</span>
					</button>
					<button class="btn btn-danger btn-xs" ng-click="deleteTask( tarea.id )"><span class="fas fa-trash"
					></span>
					</button>
		</span>
	</div>
</li>
</ul>

<div class="modal fade" id="addTask" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="AddTask">{{formtitle}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

							<form class="" name="taskForm" >

								<div class="form-group">
									<label for="taskname">Nombre de la tarea</label>
									<input type="text" class="form-control" id="taskName" ng-model='insert.name' name ="taskName" ng-minlength="5" placeholder="Ingrese el título de la tarea"  ng-required="true">


								</div>

								<div class="form-group">
									<label for="taskDescription">Descripción</label>
									<textarea class="form-control" id="taskDescription" rows="3"  ng-model='insert.description' ></textarea>
								</div>
							<div class="form-group">
							<label for="taskPriority">Prioridad</label>
								<select class="custom-select"  ng-model='insert.priority' ng-required="required">

									<option value="Baja">Baja</option>
									<option value="Media">Media</option>
									<option value="Alta">Alta</option>
							</select>
							</div>

							<div class="form-group">
								<label for="taskPriority">Fecha de vencimiento</label>
									<input type="text" class="form-control" id="datepicker" ng-required="required">
							</div>
							</form>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" ng-click='reset()'>Cerrar</button>
				<button type="button" class="btn btn-primary" id="update" ng-click='taskForm.$valid && updateTask()' ng-show="hideUpdate == 'Show'">Actualizar</button>
        <button type="button" class="btn btn-primary" id="Insert" ng-click='insertTask()' ng-show="hideInsert  == 'Show'">Guardar </button>
      </div>
    </div>
  </div>
</div>
