<table class="table table-striped table-sm" id="users-table">
				<thead>
					<tr>

						<th>Nombre</th>
						<th class="hidden-xs">Email</th>
						<th class="hidden-xs">Nivel de acceso</th>
						<th class="hidden-xs">Fecha de creación</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody>
				<?php foreach($users as $user): ?>
						<tr>
						<td><i class="fas fa-user"></i> <?php echo ucfirst($user['User']['name']); ?></td>
						<td class="hidden-xs"><i class="far fa-envelope"></i> <?php echo $user['User']['email']; ?></td>
						<td class="hidden-xs"><?php echo $user['User']['access_level']==1?'Admin':'Usuario Regular'; ?></td>
						<td class="hidden-xs"><?php echo $user['User']['created']; ?></td>
						<td>

							<?php echo $this->Html->link('<i class="far fa-edit"></i> Modificar', array('action'=>'edit',$user['User']['id']), array('class'=>'btn btn-success edit-user','escape'=>false)); ?>
							<?php echo $this->Html->link('<i class="far fa-trash-alt"></i> Borrar', array('action'=>'delete', $user['User']['id']), array('class'=>'btn btn-danger delete-user','escape'=>false)); ?>

						</td>
						</tr>
				<?php endforeach; ?>
        </tbody>
</table>
