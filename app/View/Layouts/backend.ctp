<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title><?php echo $title_for_layout; ?></title>

	<?php echo $this->Html->css(array('bootstrap.min.css','styles','https://use.fontawesome.com/releases/v5.5.0/css/all.css')); ?>
	<?php echo $this->Html->script(array('jquery-3.3.1.min.js','jquery.validate.min.js','bootstrap.bundle.min.js','https://cdn.jsdelivr.net/npm/sweetalert2@7.28.11/dist/sweetalert2.all.min.js','modal.js','scripts.js')); ?>
</head>

<body>
<nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Applicación ToDo</a>

      <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap"> <?php echo $this->Html->link('<i class="fas fa-sign-out-alt"></i> Cerrar Sesión',array('controller'=>'users','action'=>'logout','backend'=>false),array('class'=>'nav-link','escape'=>false)); ?>
        </li>
      </ul>
    </nav>

    <div class="container-fluid">
      <div class="row">
		<?php echo $this->element('Backend/sidebar'); ?>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
					<?php echo $this->Session->flash(); ?>


						<?php echo $content_for_layout; ?>

        </main>
      </div>
    </div>

</body>
</html>
