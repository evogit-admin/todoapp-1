<div class="card card-container">
            <img id="profile-img" class="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" />
            <p id="profile-name" class="profile-name-card"></p>
			<?php echo $this->Session->flash();
				  echo $this->Session->flash('auth');
			?>
            <?php echo $this->Form->create('User',array('class'=>'form-signin','url'=>array('controller'=>'users','action'=>'login'))) ?>
                <span id="reauth-email" class="reauth-email"></span>
                <input type="email" id="userEmail" class="form-control" name='data[User][email]' placeholder="Dirección de correo" required autofocus>
                <input type="password" id="userPassword" class="form-control" placeholder="Clave" name='data[User][password]' required>
                <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Ingresar</button>
            </form>

        </div>
