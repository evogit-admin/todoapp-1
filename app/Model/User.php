<?php
/**
 * Modelo de usuarios del sistema TodoApp
 *
 * @author Ing. Luis Gabriel Rodriguez <luisgabriel84@gmail.com>
 **/
App::uses('AppModel', 'Model');
class User extends AppModel {
	public $name = 'User';

	//Reglas de valicación del lado del servidor
	public $validate = array(
        'name' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'El nombre es obligatorio'
            )
		),
		'email' => array(
            'required' => array(
                'rule' => 'email',
                'message' => 'Por favor corrija su email'
			),
			'unique'=>array(
				'rule' => 'isUnique',
				'message' => 'Este Email ya ha sido registrado'
			)

        ),
        'password' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'La clave es obligatoria'
            )
        )
	);

	//Asociando el modelo de tareas para generear los joins
	 public $hasMany = array(
        'Task' => array(
            'className' => 'Task',
            'conditions' => array('Task.enabled' => '1'),
            'order' => 'Task.expiration_date DESC'
        )
    );
}
?>
