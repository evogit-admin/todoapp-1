<?php
/**
 * Modelo de tareas del sistema TodoApp
 *
 * @author Ing. Luis Gabriel Rodriguez <luisgabriel84@gmail.com>
 **/
App::uses('AppModel', 'Model');
class Task extends AppModel {
	public $name = 'Task';

	//Reglas de valicación del lado del servidor
	public $validate = array(
        'name' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'El nombre de la tarea es obligatorio'
            )
		),
		'description' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'Ingrese la descripcion de la tarea'
			),

        ),
        'expiration_date' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'Por favor ingrese una fecha'
            )
        )
	);

	//Asociando tareas con usuarios
	public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id'
        )
    );
}
?>
