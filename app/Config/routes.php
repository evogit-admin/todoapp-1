<?php

	Router::connect('/', array('controller' => 'users', 'action' => 'login'));
	Router::connect('/tasks', array('controller' => 'pages', 'action' => 'display','home'));
	Router::connect('/my-tasks', array('controller' => 'tasks', 'action' => 'index'));
	Router::connect('/insert-task', array('controller' => 'tasks', 'action' => 'add'));
	Router::connect('/delete-task', array('controller' => 'tasks', 'action' => 'delete'));
	Router::connect('/edit-task', array('controller' => 'tasks', 'action' => 'edit'));
	Router::connect('/update-task', array('controller' => 'tasks', 'action' => 'update'));
	Router::connect('/logout', array('controller' => 'users', 'action' => 'logout'));


	require CAKE . 'Config' . DS . 'routes.php';
