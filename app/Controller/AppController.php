<?php
App::uses('Controller', 'Controller');
class AppController extends Controller {

	//Inclusion de componente de autenticación
	public $components = array(
        'Session',
        'Auth' => array(
			'authenticate' => array(
				'Form' => array(
					'fields' =>
					array(
						'username' => 'email',
						'password'=>'password'
					)
				)
				),
				'authError' => 'No has iniciado session',

        )
    );

	public $helpers = array('Js');

	public function beforeFilter() {
		//Cambiar el layout para el admin
		if (isset($this->params['prefix']) && $this->params['prefix'] == 'backend') {
			$this->layout="backend";
			$user = $this->Session->read('Auth.User');
			if($user['access_level']==2){
				return $this->redirect('/tasks');
			}
		}
	}




}
