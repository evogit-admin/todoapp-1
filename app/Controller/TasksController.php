<?php
/**
 * Controlador de tareas del sistema TodoApp
 *
 * Encargado de la lógica de negocio sobre las tareas
 *
 * @author Ing. Luis Gabriel Rodriguez <luisgabriel84@gmail.com>
 **/
App::uses('AppController', 'Controller');
class TasksController extends AppController {
	/**
	 * Ejecución de acciones previo a la carga
	 */
	public function beforeFilter(){
		parent::beforeFilter();
		$user = $this->Session->read('Auth.User');

	}

	/**
	 * Listar tareas del usuario logueado
	 */
	public function index() {
		$this->layout = 'ajax';
		$this->Task->recursive = -1;
		//listar todas tareas que no se hayan vencido



		if($this->Session->check('Auth.User')){
			$user = $this->Session->read('Auth.User');
			$conditions = array('Task.user_id'=> $user['id']);
			$order = array('Task.expiration_date'=>'ASC');
			$tasks = $this->Task->find('all',array('conditions'=>$conditions,'order'=>$order));
			$tasksArray = Set::classicExtract( $tasks, '{n}.Task' );
			if(empty($tasksArray)){
				//Entregar un arreglo vacio para no enviar null
				$tasksArray = array();
			}

			$this->set('response',$tasksArray);
			$this->render('response');
		}
	}

	/**
	 *  Agregar una nueva tarea
	 */
	public function add(){
		$this->layout='ajax';
		if ($this->request->is('post')){
			$user = $this->Session->read('Auth.User');
			$data = json_decode(file_get_contents("php://input"));
			if(empty($data->name) ||  empty($data->expiration_date ) || empty($data->priority) || empty($data->description ) ){
				$response['response'] ='error';
				$response['message'] ='Revise los campos vacios del formulario';
				$this->set('response',$response);
				$this->render('response');
				return false;
			}
			$this->request->data['Task']['name'] =  $data->name;
			$this->request->data['Task']['description'] =  $data->description;
			$this->request->data['Task']['priority'] =  $data->priority;
			$this->request->data['Task']['expiration_date'] =  $data->expiration_date;
			$this->request->data['Task']['user_id'] =  $user['id'];

			$this->Task->create();
			if ($this->Task->save($this->request->data)) {
				$response['response'] ='success';
				$response['message'] ='Se guardo la tarea';
				$this->set('response',$response);
				$this->render('response');
			}else{
				$response['response'] ='error';
				$message ='No se pudo guardar la tarea debido a lo siguiente: <br/>';
				foreach($this->Task->validationErrors as $key => $error){
					foreach($error as $err){
						$message.=  $err;
					}
				}
				$response['message'] = $message;
				$this->set('response',$response);
			}
		}else{
			$response['response'] ='error';
			$response['message'] ='NO se enviaron datos';
			$this->set('response',$response);
			$this->render('response');
		}
	}

	/**
	 * Obtener los datos de una tarea para modificarlos
	 */
	public function edit(){
		$this->layout = 'ajax';
		$id = json_decode(file_get_contents("php://input"));
		$this->Task->id = $id;

        if (!$this->Task->exists()) {
            throw new NotFoundException(__('La tarea no existe'));
		}
		$this->Task->recursive=-1;
		$task = $this->Task->read(null,$id);
		$data = Set::classicExtract( $task, 'Task' );
		$this->set('response',$data);
		$this->render('response');
	}

	public function update(){
		$this->layout = 'ajax';
		$data= json_decode(file_get_contents("php://input"));
		$this->Task->id = $data->id ;

        if (!$this->Task->exists()) {
            throw new NotFoundException(__('La tarea no existe'));
		}

		if(empty($data->name) ||  empty($data->expiration_date ) || empty($data->priority) || empty($data->description ) ){
				$response['response'] ='error';
				$response['message'] ='Revise los campos vacios del formulario';
				$this->set('response',$response);
				$this->render('response');
				return false;
		}

		$user = $this->Session->read('Auth.User');
		$this->request->data['Task']['id'] =  $data->id;
		$this->request->data['Task']['name'] =  $data->name;
		$this->request->data['Task']['description'] =  $data->description;
		$this->request->data['Task']['priority'] =  $data->priority;
		$this->request->data['Task']['expiration_date'] =  $data->expiration_date;
		$this->request->data['Task']['user_id'] =  $user['id'];


		if ($this->Task->save($this->request->data)) {
			$response['response'] ='success';
			$response['message'] ='Se actualizo la tarea';
			$this->set('response',$response);
			$this->render('response');
		}else{
			$response['response'] ='error';
			$message ='No se pudo guardar la tarea debido a lo siguiente: <br/>';
			foreach($this->Task->validationErrors as $key => $error){
				foreach($error as $err){
					$message.=  $err;
				}
			}
			$response['message'] = $message;
			$this->set('response',$response);
		}
	}

	/**
	 * Eliminar una tarea de la base de datos
	 * @param id int
	 */
	public function delete(){
		$this->layout = 'ajax';
		$id = json_decode(file_get_contents("php://input"));
		$this->Task->id = $id;

        if (!$this->Task->exists()) {
            throw new NotFoundException(__('La tarea no existe'));
		}

		if($this->Task->delete($id)){
			$response['response'] ='success';
			$response['message'] ='Se elimino la tarea';
			$this->set('response',$response);
			$this->render('response');
		}
	}

}
