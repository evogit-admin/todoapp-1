/**
 * Scripts para envio de datos al servidor y
 * validación de formularios del backend
 * @autor Ing. Luis Gabriel Rodríguez <luisgabriel84@gmail.com>
 */
$(document).ready(function() {
    $('#users-form').hide();
    $('#new-user').click(function() {
        $('#users-form').toggle(200);
    });

    //Validación de formulario
    var form = $('#frmUsers');

    //Envio de datos  al servidro vía ajax al hacer click en el botón de guardar
    $("#saveUser").click(function(e) {
        e.preventDefault();
        if (form.valid() == false) {
            swal('Error en formulario', 'Por favor corrija de los campos seleccionados', 'error');
        } else {
            var userName = $('#user-name').val();
            var userEmail = $('#user-email').val();
            var userPwd = $('#user-password').val();

            $.post('/backend/users/add', {
                    name: userName,
                    email: userEmail,
                    password: userPwd
                },
                function(data) {
                    //Obtener la respuesta del servidor y mostrarselos al usuario en la alerta
                    srvResponse = $.parseJSON(data);
                    if (srvResponse.response == 'error') {
                        swal('Error al guardar el usuario', srvResponse.message, 'error');
                    } else {
                        swal('Exito', srvResponse.message, 'success')
                            .then((value) => {
                                location.reload();
                            });
                    }
                });
        }


    });
    // Borrar usuario
    $('.delete-user').click(function(e) {
        e.preventDefault();
        var url = $(this).attr('href');
        swal({
                title: "Desea borrar este usuario",
                text: "Eliminará este usuario permamente!",
                icon: "warning",
                showCloseButton: true,
                showCancelButton: true,
                cancelButtonText: 'Cancelar'

            })
            .then((result) => {
                if (result.value) {
                    window.location = url;
                }
            });
    });



    //Permitir solo letras y espacios
    $.validator.addMethod("alpha", function(value, element) {
        return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
    });

    //Reglas de validación del lado cliente para formularios
    form.validate({
        errorClass: 'is-invalid text-danger',
        successClass: 'is-valid',
        errorsWrapper: '<span class="form-text text-danger"></span>',
        errorTemplate: '<span></span>',
        wrapper: 'div',
        rules: {
            name: {
                required: true,
                alpha: true
            },
            email: {
                required: true,
                email: true,
                minlength: 10
            },
            password: {
                required: true,
                minlength: 5
            },
            passConf: {
                required: true,
                equalTo: "#user-password",
                minlength: 5
            },
            password_conf: {
                minlength: 5
            }

        },
        messages: {
            name: {
                required: "El nombre es obligatorio",
                alpha: "Solo se permiten letras y espacios en este campo"
            },

            email: {
                required: "El correo es obligatorio",
                email: "Su dirección de correo no es válida",
                minlength: 'Por lo menos 10 caracteres'
            },

            passConf: {
                required: 'Confirme su clave por favor',
                equalTo: 'Las claves no coinciden',
                minlength: 'Por lo menos 5 caracteres'
            },
            password: {
                required: 'La clave es obligatoria',
                minlength: 'Por lo menos 5 caracteres'
            },
            password_conf: {
                required: false,
                minlength: 'El nuevo password debe contener por lo menos 5 caracteres'
            }
        }
    });

});